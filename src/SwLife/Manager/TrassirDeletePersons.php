<?php
namespace SwLife\Manager;

use Doctrine\DBAL\Connection as DbConnection;
use Doctrine\DBAL\DBALException;
use SwLife\Dto\PersonDelete;
use SwLife\Entity\Trassir\Person;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TrassirDeletePersons
{
    protected $_validator;
    protected $_db;
    protected $_data = [];
    /** @var ConstraintViolationList */
    protected $_has_errors = [];

    const STAT_DELETED = 'deleted';

    public function __construct(ValidatorInterface $validator, DbConnection $db)
    {
        $this->_validator = $validator;
        $this->_db = $db;
    }

    public function setData($deleteData)
    {
        $this->_has_errors = [];
        $this->_data = [];
        if (isset($deleteData)) foreach ($deleteData as $personCardNumber) {
            $person = new PersonDelete(['card_number' => $personCardNumber]);
            /** @var ConstraintViolationList $validateErrors */
            $validateErrors = $this->_validator->validate($person);
            if (!$validateErrors->count()) {
                $this->_data[] = $person;
            } else {
                /** @var ConstraintViolation $err */
                foreach ($validateErrors as $err) {
                    $this->_has_errors[] = $err;
                }
            }
        }
    }

    public function execute()
    {
        $this->_db->beginTransaction();

        $success = true;
        $errorMessage = null;
        $stats = [static::STAT_DELETED => 0];
        try{
            foreach ($this->_data as $row) {
                if ($row instanceof PersonDelete) {
                    $res = $this->deletePerson($row);
                    $stats[$res]++;
                }
            }
            $this->_db->commit();
        }
        catch(DBALException $e) {
            $this->_db->rollback();
            $success = false;
            $errorMessage = 'Db error: ' . $e->getMessage();
        } catch(ValidatorException $e) {
            $this->_db->rollback();
            $success = false;
            $errorMessage = 'Validate Error: ' . $e->getMessage();
        }
        if (!$success) {
            return ['success' => $success, 'errorMessage' => $errorMessage];
        }
        return ['success' => $success, 'stats' => $stats];
    }

    public function deletePerson(PersonDelete $person)
    {
        $sql = "SELECT guid, \"name\", birth_date, gender, comment, folder_guid FROM persons_t WHERE guid = ?";
        $personTrassir = $this->_db->fetchAssoc($sql, [$person->card_number]);
        if (!$personTrassir) {
            throw new ValidatorException(
                sprintf('Person not found by guid = %s', $person->card_number)
            );
        }
        $personTrassir = new Person($personTrassir);
        $this->_db->delete('persons_t', ['guid' => $personTrassir->guid]);
        return static::STAT_DELETED;
    }

    public function validate()
    {
        if (!empty($this->_has_errors)) {
            return false;
        }
        return true;
    }

    public function getValidateMessages()
    {
        if (empty($this->_has_errors)) {
            return [];
        }
        $str = [];
        /** @var ConstraintViolation $err */
        foreach ($this->_has_errors as $err) {
            $str[] = sprintf('Error in person property %s: %s', $err->getPropertyPath(), $err->getMessage());
        }
        return $str;
    }
}