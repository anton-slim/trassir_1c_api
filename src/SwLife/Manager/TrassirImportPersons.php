<?php
namespace SwLife\Manager;

use Doctrine\DBAL\Connection as DbConnection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\BinaryType;
use Doctrine\DBAL\Types\Type;
use SwLife\Dto\PersonImport;
use SwLife\Entity\Trassir\Person;
use SwLife\Entity\Trassir\PersonImage;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TrassirImportPersons
{
    protected $_validator;
    protected $_db;
    protected $_data = [];
    /** @var ConstraintViolationList */
    protected $_has_errors = [];

    const STAT_INSERTED = 'inserted';
    const STAT_UPDATED = 'updated';

    public function __construct(ValidatorInterface $validator, DbConnection $db)
    {
        $this->_validator = $validator;
        $this->_db = $db;
    }

    public function setData($updateData)
    {
        $this->_has_errors = [];
        $this->_data = [];
        if (isset($updateData)) foreach ($updateData as $data) {
            $person = new PersonImport($data);
            /** @var ConstraintViolationList $validateErrors */
            $validateErrors = $this->_validator->validate($person);
            if (!$validateErrors->count()) {
                $this->_data[] = $person;
            } else {
                /** @var ConstraintViolation $err */
                foreach ($validateErrors as $err) {
                    $this->_has_errors[] = $err;
                }
            }
        }
    }

    public function execute()
    {
        $this->_db->beginTransaction();

        $success = true;
        $errorMessage = null;
        $stats = [static::STAT_INSERTED => 0, static::STAT_UPDATED => 0];
        try{
            foreach ($this->_data as $row) {
                if ($row instanceof PersonImport) {
                    $res = $this->insertOrUpdatePerson($row);
                    $stats[$res]++;
                }
            }
            $this->_db->commit();
        }
        catch(DBALException $e) {
            $this->_db->rollback();
            $success = false;
            $errorMessage = 'Db error: ' . $e->getMessage();
        } catch(ValidatorException $e) {
            $this->_db->rollback();
            $success = false;
            $errorMessage = 'Validate Error: ' . $e->getMessage();
        }
        if (!$success) {
            return ['success' => $success, 'errorMessage' => $errorMessage];
        }
        return ['success' => $success, 'stats' => $stats];
    }

    public function insertOrUpdatePerson(PersonImport $person)
    {
        /** @var \SwLife\Entity\Trassir\Person $personTrassir */
        $personTrassir = $person->convertToTrassirPerson();
        $personTrassirValidate = $this->_validator->validate($personTrassir);
        if ($personTrassirValidate->count()) {
            throw new ValidatorException(
                sprintf('Error in personTrassir property %s: %s',
                    $personTrassirValidate->get(0)->getPropertyPath(),
                    $personTrassirValidate->get(0)->getMessage()
                )
            );
        }

        $sql = "SELECT guid, \"name\", birth_date, gender, comment, folder_guid, deleted_ts FROM persons_t WHERE guid = ?";
        $oldPersonTrassir = $this->_db->fetchAssoc($sql, [$personTrassir->guid]);
        if ($oldPersonTrassir) {
            $oldPersonTrassir = new Person($oldPersonTrassir);
            $personTrassir->merge($oldPersonTrassir);
        }

        $personTrassirImages = $person->convertToTrassirImages($personTrassir);
        /** @var PersonImage $personTrassirImage */
        foreach ($personTrassirImages as $personTrassirImage) {
            $personTrassirImageValidate = $this->_validator->validate($personTrassirImage);
            if ($personTrassirImageValidate->count()) {
                throw new ValidatorException(
                    sprintf('Error in personTrassirImage property %s: %s',
                        $personTrassirImageValidate->get(0)->getPropertyPath(),
                        $personTrassirImageValidate->get(0)->getMessage()
                    )
                );
            }
        }

        // ставим изображение по умолчанию
        if ($personTrassirImages) {
            $personTrassir->setImageGuid($personTrassirImages[0]->image_guid);
        } else {
            $personTrassir->setImageGuid(null);
        }

        $ret = null;
        // обновление персоны
        if ($oldPersonTrassir) {
            $this->_db->delete('fr_enrolled_persons_t', ['person_guid' => $personTrassir->guid]);
            $this->_db->update('persons_t', $personTrassir->toArray(), ['guid' => $personTrassir->guid]);
            $ret = static::STAT_UPDATED;
        } else { // новая запись
            $personTrassir->comment = $personTrassir->guid;
            $this->_db->insert('persons_t', $personTrassir->toArray());
            $ret = static::STAT_INSERTED;
        }

        // импорт изображений персоны
        // удаляем старые
        $this->_db->delete('persons_images_t', ['person_guid' => $personTrassir->guid]);
        /** @var PersonImage $personTrassirImage */
        foreach ($personTrassirImages as $personTrassirImage) {
            // загружаем новые
            $this->_db->insert('persons_images_t', $personTrassirImage->toArray(), ['image' => Type::BINARY, 'thumbnail' => Type::BINARY]);
        }
        return $ret;
    }

    public function validate()
    {
        if (!empty($this->_has_errors)) {
            return false;
        }
        return true;
    }

    public function getValidateMessages()
    {
        if (empty($this->_has_errors)) {
            return [];
        }
        $str = [];
        /** @var ConstraintViolation $err */
        foreach ($this->_has_errors as $err) {
            $str[] = sprintf('Error in person property %s: %s', $err->getPropertyPath(), $err->getMessage());
        }
        return $str;
    }
}