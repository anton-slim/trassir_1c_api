<?php

namespace SwLife\Dto;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use SwLife\Entity\Trassir\Person as TrassirPerson;


class PersonDelete extends AbstractDto1c
{
    public $card_number;


    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('card_number', new Assert\Regex('!^[0-9]+$!'));
    }
}