<?php

namespace SwLife\Dto;

use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use SwLife\Entity\Trassir\Person as TrassirPerson;
use SwLife\Entity\Trassir\PersonImage as TrassirPersonImage;


class PersonImport extends AbstractDto1c {
    public $name;
    public $card_number;
    public $birthday;
    public $gender;
    public $folder;
    public $images;
    public $guid;

    const VALID_MIME_IMAGES = ['image/jpeg', 'image/jpg'];


    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new Assert\NotBlank());
        $metadata->addPropertyConstraint('card_number', new Assert\Regex('!^[0-9]+$!'));
        $metadata->addPropertyConstraint('birthday', new Assert\Regex('!^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$!'));
        $metadata->addPropertyConstraint('gender', new Assert\Choice(['М', 'Ж']));
        $metadata->addPropertyConstraint('images', new Assert\Callback(function($images, ExecutionContext $context) {
            if (!$images) {
                return;
            }
            foreach ($images as $img) {
                $imgBinary = base64_decode($img, true);
                if (!$imgBinary) {
                    $context->buildViolation('Not a valid base64')
                        ->addViolation();
                    return;
                }
                $finfo = new \finfo(\FILEINFO_MIME_TYPE);
                $mime_type = $finfo->buffer($imgBinary);
                if (!in_array($mime_type, PersonImport::VALID_MIME_IMAGES)) {
                    $context->buildViolation('Not a valid image')
                        ->addViolation();
                    return;
                }
            }
        }));
        //$metadata->addPropertyConstraint('folder', new Assert\NotBlank());
    }

    public function convertToTrassirPerson()
    {
        return new TrassirPerson(array(
            'guid' => $this->card_number,
            'name' => $this->name,
            'birth_date' => $this->mapDate($this->birthday),
            'gender' => $this->mapGender($this->gender),
            'folder_guid' => $this->mapFolder($this->folder),
        ));
    }

    public function convertToTrassirImages(TrassirPerson $person)
    {
        $images = [];
        foreach ($this->images as $image) {
            $images[] = new TrassirPersonImage(array(
                'person_guid' => $person->guid,
                'image' => base64_decode($image, true),
            ));
        }
        return $images;
    }

    protected function mapGender($gender)
    {
        switch ($gender) {
            case 'М': return '1';
            case 'Ж': return '2';
        }
        return '3';
    }

    protected function mapDate($date)
    {
        return implode('-', array_reverse(explode('.', $date)));
    }

    protected function mapFolder($folder)
    {
        return $this->findFolderGuid($folder) ?? app('config')['swlife']['default_folder'];
    }

    protected function findFolderGuid($folder)
    {
        return null;
    }
}