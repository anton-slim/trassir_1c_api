<?php
namespace SwLife\Entity\Trassir;

abstract class AbstractEntityTrassir
{
    public function __construct(array $data)
    {
        foreach ($data as $key => $val) {
            $this->$key = $val;
        }
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    protected static function makeGuid()
    {
        $microtime = explode('.', microtime(1));
        return base_convert($microtime[0] . $microtime[1], 10, 36);
    }
}