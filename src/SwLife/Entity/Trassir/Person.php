<?php

namespace SwLife\Entity\Trassir;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;


class Person extends AbstractEntityTrassir
{
    public $guid;
    public $name;
    public $birth_date;
    public $gender;
    public $folder_guid;
    public $comment;
    public $image_guid;
    public $image_change_ts;
    public $deleted_ts;


    public function __construct(array $data)
    {
        parent::__construct($data);
        if (!isset($this->guid)) {
            $this->guid = self::makeGuid();
        }
    }

    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('guid', new Assert\NotBlank());
        $metadata->addPropertyConstraint('name', new Assert\NotBlank());
        $metadata->addPropertyConstraint('birth_date', new Assert\Regex('!^[0-9]{4}-[0-9]{2}-[0-9]{2}$!'));
        $metadata->addPropertyConstraint('gender', new Assert\Choice([1, 2, 3]));
        $metadata->addPropertyConstraint('folder_guid', new Assert\NotBlank());
    }

    public function merge(Person $personTrassir)
    {
        if (isset($personTrassir->guid)) {
            $this->guid = $personTrassir->guid;
        }
        if (!isset($this->guid) || $this->guid == '') {
            $this->guid = self::makeGuid();
        }
        if (isset($personTrassir->folder_guid) && $personTrassir->folder_guid != '' && empty($personTrassir->deleted_ts)) {
            $this->folder_guid = $personTrassir->folder_guid;
        }
        if (isset($personTrassir->comment) && $personTrassir->comment != '') {
            $this->comment = $personTrassir->comment;
        } else {
            $this->comment = $this->guid;
        }
        $this->deleted_ts = null;
    }

    public function setImageGuid($imageGuid)
    {
        // обнуляем текущее значение
        if (!isset($imageGuid) || $imageGuid == '') {
            $this->image_guid = null;
            $this->image_change_ts = null;
            return;
        }

        // ставим новые
        $this->image_guid = $imageGuid;
        $time = explode(' ', microtime());
        $micro_seconds = explode('.', $time[0]);
        $micro_seconds = substr($micro_seconds[1], 0, 6);
        $this->image_change_ts = $time[1] . $micro_seconds;
    }
}