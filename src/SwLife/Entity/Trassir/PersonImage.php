<?php

namespace SwLife\Entity\Trassir;

use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class PersonImage extends AbstractEntityTrassir
{
    public $person_guid;
    public $image_guid;
    public $image;
    public $thumbnail;

    const VALID_MIME_IMAGES = ['image/jpeg', 'image/jpg'];

    public function __construct(array $data)
    {
        parent::__construct($data);
        if (!isset($this->image_guid)) {
            $this->image_guid = self::makeGuid();
        }
        if (isset($this->image) && !isset($this->thumbnail)) {
            $this->setImageThumbnail($this->image);
        } else {
            $this->thumbnail = null;
        }
    }

    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $image_callback = new Assert\Callback(function($imgBinary, ExecutionContext $context) {
            if (!$imgBinary) {
                $context->buildViolation('Not a valid binary')
                    ->addViolation();
                return false;
            }
            $finfo = new \finfo(\FILEINFO_MIME_TYPE);
            $mime_type = $finfo->buffer($imgBinary);
            if (!in_array($mime_type, PersonImage::VALID_MIME_IMAGES)) {
                $context->buildViolation('Not a valid image')
                    ->addViolation();
                return false;
            }
        });

        $metadata->addPropertyConstraint('person_guid', new Assert\NotBlank());
        $metadata->addPropertyConstraint('image_guid', new Assert\NotBlank());
        $metadata->addPropertyConstraint('image', $image_callback);
        $metadata->addPropertyConstraint('thumbnail', $image_callback);
    }

    protected static function makeImageThumbnail($imageBlob, $size = 200, $cropZoom = true)
    {
        $imagick = new \Imagick();
        $imagick->readImageBlob($imageBlob);

        $imagick->setCompressionQuality(100);
        $imagick->setImageCompressionQuality(100);
        $imagick->resizeImage($size, $size,\Imagick::FILTER_LANCZOS,1, true);

        //$imagick->thumbnailImage($size, $size, true, true);

        //file_put_contents('file_' . md5($imagick->getImageBlob()) . '.jpg', $imagick->getImageBlob());
        return $imagick->getImageBlob();
    }

    public function setImageThumbnail($imageBlob)
    {
        if (isset($imageBlob) && $imageBlob != '') {
            $this->thumbnail = self::makeImageThumbnail($imageBlob);
        } else {
            $this->thumbnail = null;
        }
    }
}