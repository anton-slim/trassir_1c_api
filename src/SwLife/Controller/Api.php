<?php
namespace SwLife\Controller;

use Silicone\Route;
use Silicone\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SwLife\Manager\TrassirImportPersons;
use SwLife\Manager\TrassirDeletePersons;

class Api extends Controller
{
    /**
     * @var \Application
     */
    protected $app;


    public function getJsonContent($key = null)
    {
        $content = $this->request->getContent();
        $json = json_decode($content, true);
        return $key !== null ? ($json[$key] ?? null) : $json;
    }

    /**
     * @Route("/persons/update", name="persons_update")
     * @return string
     */
    public function updateAction(Request $request)
    {
        $updateData = $this->getJsonContent();
        if (!$request->isMethod(Request::METHOD_POST) || !$updateData) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Empty data'));
        }
        if (!is_array($updateData)) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Not array data'));
        }
        $manager = new TrassirImportPersons($this->app['validator'], $this->app['db']);
        $manager->setData($updateData);

        if (!$manager->validate()) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Validation error', 'errorMessage' => implode('.' . PHP_EOL, $manager->getValidateMessages())));
        }
        $result = $manager->execute();
        if (!$result['success']) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Update data error', 'errorMessage' => $result['errorMessage']));
        }
        return $this->app->response_json(array('status' => 1,
            'message' => sprintf('Update data success. Inserted: %d. Updated: %d',
                $result['stats'][TrassirImportPersons::STAT_INSERTED],
                $result['stats'][TrassirImportPersons::STAT_UPDATED]
            )
        ));
    }

    /**
     * @Route("/persons/delete", name="persons_delete")
     * @return string
     */
    public function deleteAction(Request $request)
    {
        $deleteData = $this->getJsonContent();
        if (!$request->isMethod(Request::METHOD_POST) || !$deleteData) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Empty data'));
        }
        if (!is_array($deleteData)) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Not array data'));
        }

        $manager = new TrassirDeletePersons($this->app['validator'], $this->app['db']);
        $manager->setData($deleteData);

        if (!$manager->validate()) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Validation error', 'errorMessage' => implode('.' . PHP_EOL, $manager->getValidateMessages())));
        }
        $result = $manager->execute();
        if (!$result['success']) {
            return $this->app->response_json(array('status' => 0, 'message' => 'Delete data error', 'errorMessage' => $result['errorMessage']));
        }
        return $this->app->response_json(array('status' => 1,
            'message' => sprintf('Delete data success. Deleted: %d',
                $result['stats'][TrassirDeletePersons::STAT_DELETED]
            )
        ));
    }

}
