<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class Application extends Silicone\Application
{
    public function getInstance()
    {

    }

    public function __construct(array $values = array())
    {
        parent::__construct($values);
    }

    protected function configure()
    {
        $app = $this;
        require_once $app->getRootDir() . '/app/config/prod.php';
    }

    /**
     * Get writeable directory.
     * @return string
     */
    public function getOpenDir()
    {
        static $dir;
        if (empty($dir)) {
            $dir = $this->getRootDir() . '/data/';
        }
        return $dir;
    }

    /**
     * Main run method with HTTP Cache.
     *
     * @param Request $request
     */
    public function run(Request $request = null)
    {
        if ($this['debug']) {
            parent::run($request);
        } else {
            $this['http_cache']->run($request);
        }
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function entityManager()
    {
        return $this['em'];
    }

    /**
     * Get session object.
     *
     * @return Session
     */
    public function session()
    {
        return $this['session'];
    }

    /**
     * @param string $role
     * @return bool
     */
    public function isGranted($role)
    {
        return $this['security']->isGranted($role);
    }


    public function response_pixel(\Symfony\Component\HttpFoundation\Cookie $cookie = null) {
        $xgif = base64_decode('R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==');
        $response = new Response($xgif, 200, array(
            'Content-Type' => 'image/gif',
            'Pragma' => 'public',
            'Expires' => 0,
            'Accept-Ranges' => 'bytes',
            'Content-length' => strlen($xgif),
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
        ));
        if (null !== $cookie) {
            $response->headers->setCookie($cookie);
        }
        return $response;
    }

    public function response_json($array = array(), $callback = null, $cookie = null) {
        $body = json_encode($array);
        if (null !== $callback) {
            $body = $callback . '(' . $body . ')';
        }
        $response = new Response($body, 200, array(
            'Content-Type' => 'text/javascript',
            'Pragma' => 'public',
            'Expires' => 0,
            'Accept-Ranges' => 'bytes',
            'Content-length' => mb_strlen($body),
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
        ));
        if (null !== $cookie) {
            $response->headers->setCookie($cookie);
        }
        return $response;
    }


}

function camelCase($string, $capitalizeFirstCharacter = false)
{

    $str = str_replace(array(' ', '-'), '', ucwords($string));

    if (!$capitalizeFirstCharacter) {
        $str = lcfirst($str);
    }

    return $str;
}

/**
 * Generate UniqueID
 */
function makeUuid()
{
    // b6732e33-7ac5-11e1-b49b-50e5495829ef
    $uniqueid = mb_substr(sha1(uniqid(rand(), 1)), 0, 8)
        . '-' . mb_substr(sha1(uniqid(rand(), 1)), 0, 4)
        . '-' . mb_substr(sha1(uniqid(rand(), 1)), 0, 4)
        . '-' . mb_substr(sha1(uniqid(rand(), 1)), 0, 4)
        . '-' . mb_substr(sha1(uniqid(rand(), 1)), 0, 12);
    return $uniqueid;
}

function makeGUID()
{
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}
