<?php
/** @var $app Application */

require_once __DIR__ . '/config.php';

$app['debug'] = false;
$app['monolog.level'] = \Monolog\Logger::WARNING;
