<?php
/** @var $app Application */

// YML settings
$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/settings.yml'));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $app['config']['database']
));

// Router
$app->register(new Silicone\Provider\RouterServiceProvider());
$app['router.resource'] = array(
    $app->getRootDir() . '/src/SwLife/Controller/',
);
$app['router.cache_dir'] = $app->getCacheDir();

// Assets
$app['assets.base_path'] = '/web/';

// Http Cache
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => $app->getCacheDir() . '/http/',
));

// Controllers
$app['resolver'] = $app->share(function () use ($app) {
    return new Silicone\Controller\ControllerResolver($app, $app['logger']);
});
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

// Validator
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Doctrine Common
$app->register(new Silicone\Provider\DoctrineCommonServiceProvider());

// Monolog
$app->register(new Silex\Provider\MonologServiceProvider());
$app['monolog.logfile'] = $app->getLogDir() . '/log.txt';
$app['monolog.name'] = 'Silicone';

// Session
$app->register(new Silex\Provider\SessionServiceProvider(), array(
    'session.storage.options' => array(
        'name' => 'Silicone',
    )
));
