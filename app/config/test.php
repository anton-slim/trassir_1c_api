<?php
/** @var $app Application */

require_once __DIR__ . '/config.php';

$app['debug'] = true;
$app['monolog.level'] = \Monolog\Logger::NOTICE;