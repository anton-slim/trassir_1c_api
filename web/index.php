<?php

require_once __DIR__ . '/../app/bootstrap.php';

$app = app_instance();
$app->run();

function app_instance() {
    static $app;
    if ($app === null) {
        $app = new Application();
    }
    return $app;
}

function app($key = null) {
    $app = app_instance();
    if ($key === null) {
        return $app;
    }
    return $app[$key];
}